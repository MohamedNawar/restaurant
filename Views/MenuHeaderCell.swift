//
//  TableViewCell.swift
//  restaurant
//
//  Created by iMac on 2/13/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class MenuHeaderCell: UITableViewCell {
    var detailsCallBack: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func openAndClose(_ sender: Any) {
        self.detailsCallBack!()
        
    }
    
}
