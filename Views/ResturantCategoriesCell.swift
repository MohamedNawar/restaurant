//
//  ResturantCategoriesCell.swift
//  restaurant
//
//  Created by Mohamed Hassan Nawar on 2/14/19.
//  Copyright © 2019 iMac. All rights reserved.
//


import UIKit
import Alamofire
import PKHUD
import FCAlertView
class ResturantCategoriesCell:  UITableViewCell, UICollectionViewDataSource , UICollectionViewDelegate {
    var categoryCallBack: ((Int) -> Void)?
    let array1 = ["Burger Sandwish","Burger","Appetizers","Appetizers","Burger Sandwish","Burger"]

    let array2 = [#imageLiteral(resourceName: "burger-2"),#imageLiteral(resourceName: "burger-2"),#imageLiteral(resourceName: "popcorn-bag"),#imageLiteral(resourceName: "popcorn-bag"),#imageLiteral(resourceName: "burger-2"),#imageLiteral(resourceName: "burger-2")]
    @IBOutlet weak var CategoriesCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        CategoriesCollectionView.delegate = self
        CategoriesCollectionView.dataSource = self
        let size = array1.first?.size(withAttributes:[.font: UIFont.systemFont(ofSize:14.0)])

        print("\(size)")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array1.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectCategoryCell", for: indexPath)
        var img = cell.viewWithTag(4) as! UIImageView
        img.image = array2[indexPath.row]
        let lbl = cell.viewWithTag(21) as! UILabel
        lbl.text = array1[indexPath.row]

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        CategoriesCollectionView.cellForItem(at: indexPath)?.borderColor = #colorLiteral(red: 0.06097269803, green: 0.53157866, blue: 0.4588555098, alpha: 1)
        categoryCallBack?(indexPath.row)
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        CategoriesCollectionView.cellForItem(at: indexPath)?.borderColor = #colorLiteral(red: 0.5741485357, green: 0.5741624236, blue: 0.574154973, alpha: 1)
        CategoriesCollectionView.cellForItem(at: indexPath)?.reloadInputViews()
    }
    
}

extension ResturantCategoriesCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        for i in 0..<array1.count {
            if indexPath.row == i {
                let size = array1[i].size(withAttributes:[.font: UIFont.systemFont(ofSize:14.0)])
                let width = size.width + 55
            return CGSize(width: width, height: 50)
            }
        }
        return CGSize(width: 50, height: 50)
    }
}
