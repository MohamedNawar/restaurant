//
//  ViewController.swift
//  Scarfi
//
//  Created by iMac on 11/19/18.
//  Copyright © 2018 iMac. All rights reserved.
//
//
//  LoginViewController.swift
//  YourSchools
//
//  Created by iMac on 11/4/18.
//  Copyright © 2018 iMac. All rights reserved.
//
import Foundation
struct userData : Decodable {
    static var Instance = userData()
    private init() {}
    var data : UserDetails?
    var token : String?
    var firstUse:String?
    
    func saveUser(data:Data) {
        UserDefaults.standard.set(data, forKey: "user")
    }
    
    mutating func remove() {
        UserDefaults.standard.removeObject(forKey: "user")

        token = ""
    }
    
    mutating func removeIdentifierInside() {
        UserDefaults.standard.removeObject(forKey: "identifier")
    }
    
    mutating func fetchUser(){
        if let  data = UserDefaults.standard.data(forKey: "user") {
            do {
                self = try JSONDecoder().decode(userData.self, from: data)
            }catch{
                print("hey check me out!!")
            }
        }
        if let  data = UserDefaults.standard.string(forKey: "firstUse") {
            self.firstUse = String(data)
        }
    }
}
struct CurrentUser:Decodable {
    var data: UserDetails?
}
struct UserDetails : Decodable {
    var id:Int?
    var name : String?
    var email:String?
    var phone_number:String?
    var type:String?
    var avatar: String?
    var last_name: String?
    var first_name: String?
    var verified: Bool?
    var ability: AbilityData?
}
struct AbilityData:Decodable {
    var branches: AbilityBool?
    var categories: AbilityBool?
    var custom_attributes: AbilityBool?
    var products: AbilityBool?
}
struct AbilityBool: Decodable {
    var create: Bool?
    var index: Bool?
}




