//
//  MyOrderStateVC.swift
//  restaurant
//
//  Created by iMac on 2/20/19.
//  Copyright © 2019 iMac. All rights reserved.
//

    import PKHUD
    import Alamofire
    import FCAlertView
    import SDWebImage
    import HCSStarRatingView
    
    class MyOrderStateVC: UIViewController,UITableViewDelegate, UITableViewDataSource   , UINavigationBarDelegate, UICollectionViewDataSource , UICollectionViewDelegate{
        let array1 = ["Burger Sandwish","Burger","Appetizers"]
        
        @IBOutlet weak var StateCollectionView: UICollectionView!
        @IBOutlet weak var MyOrdersTable: UITableView!

        override func viewDidLoad() {
            super.viewDidLoad()
            MyOrdersTable.separatorStyle = .none
            MyOrdersTable.allowsSelection = false
            userData.Instance.fetchUser()
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return array1.count
        }
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            var cell = UICollectionViewCell()
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectCategoryCell", for: indexPath)
            let lbl = cell.viewWithTag(21) as! UILabel
            lbl.text = array1[indexPath.row]
            return cell
        }
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            StateCollectionView.cellForItem(at: indexPath)?.borderColor = #colorLiteral(red: 0.4470946193, green: 0.6766976118, blue: 0.9975687861, alpha: 1)
        }
        func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
            StateCollectionView.cellForItem(at: indexPath)?.borderColor = #colorLiteral(red: 0.5741485357, green: 0.5741624236, blue: 0.574154973, alpha: 1)
            StateCollectionView.cellForItem(at: indexPath)?.reloadInputViews()
        }
        override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "Group -2");self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "Group -2")
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
            return 7
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let  cell = MyOrdersTable.dequeueReusableCell(withIdentifier: "MyOrderCell", for: indexPath)
            
            return cell
        }
}


extension MyOrderStateVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        for i in 0..<array1.count {
            if indexPath.row == i {
                let size = array1[i].size(withAttributes:[.font: UIFont.systemFont(ofSize:16.0)])
                let width = size.width + 30
                return CGSize(width: width, height: 50)
            }
        }
        return CGSize(width: 50, height: 50)
    }
}
