//
//  DetailsForOrderVC.swift
//  restaurant
//
//  Created by Mohamed Hassan Nawar on 2/21/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit
import TextFieldEffects

class DetailsForOrderVC: UIViewController {
    @IBOutlet weak var CashImg: UIImageView!
    @IBOutlet weak var VisaImg: UIImageView!
    @IBOutlet weak var addressTxt: HoshiTextField!
    @IBOutlet weak var buildTxt: HoshiTextField!
    @IBOutlet weak var phoneTxt: HoshiTextField!
    @IBOutlet weak var commentTxt: HoshiTextField!
    @IBOutlet weak var visaLbl: UILabel!
    
    @IBOutlet weak var cashSelectView: UIView!
    @IBOutlet weak var visaSelectView: UIView!
    @IBOutlet weak var cashLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func Visa(_ sender: Any) {
        visaLbl.textColor = #colorLiteral(red: 0.4470946193, green: 0.6766976118, blue: 0.9975687861, alpha: 1)
        visaSelectView.isHidden = false
        VisaImg.image = #imageLiteral(resourceName: "credit-card (1)")
        cashLbl.textColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
        cashSelectView.isHidden = true
        CashImg.image = #imageLiteral(resourceName: "purse")
    }
    @IBAction func Cash(_ sender: Any) {
        CashImg.image = #imageLiteral(resourceName: "purse (1)")
       cashLbl.textColor = #colorLiteral(red: 0.4470946193, green: 0.6766976118, blue: 0.9975687861, alpha: 1)
        cashSelectView.isHidden = false
        VisaImg.image = #imageLiteral(resourceName: "credit-card")
        visaLbl.textColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
        visaSelectView.isHidden = true

    }
    
}
