//
//  ResturantVC.swift
//  restaurant
//
//  Created by iMac on 2/17/19.
//  Copyright © 2019 iMac. All rights reserved.
//

    import UIKit
    import PKHUD
    import Alamofire
    import FCAlertView
    import SDWebImage
    import Auk
    import moa
    import HCSStarRatingView

    class ResturantVC: UIViewController,UITableViewDelegate, UITableViewDataSource   , UINavigationBarDelegate{
     
      
       
        @IBOutlet weak var ResturantTable: UITableView!

        @IBOutlet weak var scrollView: UIScrollView!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            scrollView.auk.settings.contentMode = .scaleToFill
            ResturantTable.separatorStyle = .none
            ResturantTable.allowsSelection = false
            userData.Instance.fetchUser()
            
       }
//        func setScrollView(product:productCategory){
//            if let images = product.images {
//                for image in images {
//                    scrollView.auk.show(url:(image.src) ?? "")
//                }
//            }
//        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
            return 5
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            var cell = UITableViewCell()
            if indexPath.row == 0 {
                let cell1 = ResturantTable.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath)
                
                return cell1
            }
            if indexPath.row == 1 {
                cell = ResturantTable.dequeueReusableCell(withIdentifier: "RattingCell", for: indexPath)
                //                let lbl1 = cell.viewWithTag(1) as! UILabel
                //                let lbl2 = cell.viewWithTag(2) as! UILabel
                //                let lbl3 = cell.viewWithTag(3) as! UILabel
                //                let lbl4 = cell.viewWithTag(4) as! UILabel
                //                let lbl5 = cell.viewWithTag(5) as! UILabel
                //                let lbl6 = cell.viewWithTag(6) as! UILabel
                //                let lbl7 = cell.viewWithTag(7) as! UILabel
                //                let lbl8 = cell.viewWithTag(8) as! UILabel
                //                let ratting = cell.viewWithTag(10) as! HCSStarRatingView
                
                return cell
            }
           cell =  ResturantTable.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath)
            
            return cell
        }
        func loadData(id:Int){
            let header = APIs.Instance.getHeader()
            HUD.show(.progress, onView: self.view)
            print(APIs.Instance.Showproduct(id: id))
            Alamofire.request(APIs.Instance.Showproduct(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        do {
                            let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                            self.makeDoneAlert(title: "Error", SubTitle: err.message ?? "", Image:  #imageLiteral(resourceName: "danger"))
                            print(err.message)
                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                        do {
//                            self.productData = try JSONDecoder().decode(productCategory.self, from: response.data!)
//                            self.setScrollView(product: self.productData); print(self.productData.extra_fields?.comment?.comment_author ?? "")
                        }catch{
                            print(error)
                            let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                            HUD.flash(.labeledError(title: "", subtitle: lockString), delay: 1.0)
                        }
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
            }
        }
        func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
            let alert = FCAlertView()
            alert.avoidCustomImageTint = true
            let updatedFrame = alert.bounds
            alert.colorScheme = #colorLiteral(red: 0.5663285255, green: 0.7342554331, blue: 0.9790433049, alpha: 1)
            alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: ""), andButtons: nil)
        }

  
}


