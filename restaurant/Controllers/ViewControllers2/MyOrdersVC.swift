//
//  MyOrdersVC.swift
//  restaurant
//
//  Created by iMac on 2/19/19.
//  Copyright © 2019 iMac. All rights reserved.
//




import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
import HCSStarRatingView

class MyOrdersVC: UIViewController,UITableViewDelegate, UITableViewDataSource   , UINavigationBarDelegate{
    
    @IBOutlet weak var MyOrdersTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MyOrdersTable.separatorStyle = .none
        MyOrdersTable.allowsSelection = false
        userData.Instance.fetchUser()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "Group -2");self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "Group -2")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 7
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let  cell = MyOrdersTable.dequeueReusableCell(withIdentifier: "MyOrderCell", for: indexPath)
        
        return cell
    }
}


