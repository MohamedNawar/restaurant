//
//  BranchesVC.swift
//  restaurant
//
//  Created by iMac on 2/13/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import SideMenu
import FCAlertView
class BranchesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var branches = Branches(){
        didSet{
            branchesTable.reloadData()
        }
    }
    @IBOutlet weak var branchesTable: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        userData.Instance.fetchUser()
        getBranches()
        self.tabBarController?.navigationItem.hidesBackButton = true
        branchesTable.clipsToBounds = true; self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        branchesTable.separatorStyle = .none
    self.navigationItem.leftItemsSupplementBackButton = true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navVC =  AppStoryboard.Main2.viewController(viewControllerClass: ResturantMenu2VC.self)
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.hidesBackButton = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return branches.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "branchCell", for: indexPath)
        let lbl = cell.viewWithTag(1) as! UILabel
        lbl.text = branches.data?[indexPath.row].name ?? ""
        let view = cell.viewWithTag(22) as! UIView
        return cell
    }

    private func getBranches(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress)
        Alamofire.request(APIs.Instance.getBranches() , method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                //HUD.hide()
                //print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
//                        self.makeDoneAlert(title: "Error", SubTitle: err.errors?.first ?? "", Image:  #imageLiteral(resourceName: "home"))
                        print(err.message)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.branches = try JSONDecoder().decode(Branches.self, from: response.data!)
                        print("successsss")
                        HUD.hide()
                    }catch{
                        print(error.localizedDescription)
                        HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "حدث خطأ برجاء اعادة المحاولة")), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
