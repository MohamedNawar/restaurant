//
//  TabBarC.swift
//  restaurant
//
//  Created by iMac on 2/17/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class TabBarC:UITabBar {
    
    override func awakeFromNib() {
    super.awakeFromNib()
    
    }
    
    override public func sizeThatFits(_ size: CGSize) -> CGSize {
    var sizeThatFits = super.sizeThatFits(size)
    sizeThatFits.height = 50 + safeAreaInsets.bottom
    return sizeThatFits
    }
    
}
