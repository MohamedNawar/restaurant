//
//  OrderDetailsVC.swift
//  restaurant
//
//  Created by Mohamed Hassan Nawar on 2/21/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class OrderDetailsVC: UIViewController {

    @IBOutlet weak var orderImg: UIImageView!
    @IBOutlet weak var totalPriceLbl: UILabel!
    @IBOutlet weak var additionPriceLbl: UILabel!
    @IBOutlet weak var orderPriceLbl: UILabel!
    @IBOutlet weak var additionDetailsLbl: UILabel!
    @IBOutlet weak var orderDetailsLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var orderNumLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func reorder(_ sender: Any) {
    }
    
    @IBAction func dismiss(_ sender: Any) {
    }
}
