//
//  ImagesVC.swift
//  restaurant
//
//  Created by iMac on 2/19/19.
//  Copyright © 2019 iMac. All rights reserved.
//

    import UIKit
    import PKHUD
    import Alamofire
    import FCAlertView
    import SDWebImage

    class ImagesVC: UIViewController, UICollectionViewDataSource , UICollectionViewDelegate    {
       
        
        @IBOutlet weak var SectionsCollectionView: UICollectionView!
        override func viewDidLoad() {
            super.viewDidLoad()
            userData.Instance.fetchUser()
            SectionsCollectionView.delegate = self
            SectionsCollectionView.dataSource = self
           
        }
        override func viewWillAppear(_ animated: Bool) {
            self.tabBarController?.tabBar.tintColor = #colorLiteral(red: 0.4470946193, green: 0.6766976118, blue: 0.9975687861, alpha: 1); self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "Group -3")
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "Group -3")
        }
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 6
            
        }
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = SectionsCollectionView.dequeueReusableCell(withReuseIdentifier: "SectionsCell1", for: indexPath)
       
            return cell
        }
}
    extension ImagesVC: UICollectionViewDelegateFlowLayout{
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 0, left: 0 , bottom: 0, right: 0)
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0.0
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 0.0
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let width = (self.SectionsCollectionView.frame.width/2)
            return CGSize(width: width, height: width*4/3)
        }
}



