//
//  ResturantMenu2VC.swift
//  restaurant
//
//  Created by iMac on 2/13/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
import HCSStarRatingView
struct cellData {
    var open = Bool()
    var title = String()
}
class ResturantMenu2VC: UIViewController,UITableViewDelegate, UITableViewDataSource   , UINavigationBarDelegate{
    
    @IBOutlet weak var ResturantMenuTable: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        ResturantMenuTable.separatorStyle = .none
        ResturantMenuTable.allowsSelection = false
        userData.Instance.fetchUser()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "Group -2");self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "Group -2")
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 7
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
      
        if indexPath.row == 0 {
            let cell1 = ResturantMenuTable.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! ResturantCategoriesCell
            return cell1
        }
        
        cell = ResturantMenuTable.dequeueReusableCell(withIdentifier: "OrdersCell", for: indexPath)
        
        return cell
    }
}


