//
//  OrderProcessVC.swift
//  restaurant
//
//  Created by iMac on 2/19/19.
//  Copyright © 2019 iMac. All rights reserved.
//

    import UIKit
    import PKHUD
    import Alamofire
    import FCAlertView
    import SDWebImage
    class OrderProcessVC: UIViewController , UITableViewDelegate , UITableViewDataSource {
        var Menu = [MenuData](){
            didSet{
                OrderTableView.reloadData()
            }
        }
        @IBOutlet weak var OrderTableView: UITableView!
        var isExpanded = false
        override func viewDidLoad() {
            super.viewDidLoad()
            var m1 = MenuData()
            var m2 = MenuData()
            m1.tag = false
            m2.tag = false
            Menu.append(m1)
            Menu.append(m2)
            OrderTableView.separatorStyle = .none
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            
            return Menu.count ?? 0
            
        }
        
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if Menu[section].tag == true {
                let count = 2
                return count+2
            }else{
                return 1
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            var cell1 = UITableViewCell()
            if indexPath.row == 0 {
                let cell11 = OrderTableView.dequeueReusableCell(withIdentifier: "HeaderOrdersCell", for: indexPath) as! MenuHeaderCell
                let view = cell11.viewWithTag(101) as! UIView
                let view1 = cell11.viewWithTag(10) as! UIView
                let btn = cell11.viewWithTag(110) as! UIButton
                
                if self.Menu[indexPath.section].tag == true {
                    view.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    view1.borderColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
                    view1.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                    btn.setImage(#imageLiteral(resourceName: "next-1"), for: .normal)
                }else{
                    view.borderColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
                    view1.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                    view1.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                    
                    btn.setImage(#imageLiteral(resourceName: "next"), for: .normal)
                }
                cell11.detailsCallBack = {
                    () in
                    if self.Menu[indexPath.section].tag == true {
                        self.Menu[indexPath.section].tag = false
                        view.borderColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
                        view1.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                        btn.setImage(#imageLiteral(resourceName: "next"), for: .normal)
                    }else{
                        self.Menu[indexPath.section].tag = true
                        view.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                        view1.borderColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
                        btn.setImage(#imageLiteral(resourceName: "next-1"), for: .normal)
                    }
                    self.OrderTableView.reloadData()
                }
                return cell11
            }
            if indexPath.row == (3) {
                let cell10 = OrderTableView.dequeueReusableCell(withIdentifier: "FooterOrdersCell", for: indexPath)
                return cell10
                
            }else{
                cell1 = OrderTableView.dequeueReusableCell(withIdentifier: "OrdersCell", for: indexPath)
                return cell1
            }
        }
        
}
