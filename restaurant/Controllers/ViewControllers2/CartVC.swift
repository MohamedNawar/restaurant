//
//  CartVC.swift
//  restaurant
//
//  Created by iMac on 2/19/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class CartVC: UIViewController , UITableViewDelegate , UITableViewDataSource {
 
    

    @IBOutlet weak var CartTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    //    CartTable.separatorStyle = .none
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "Group -2")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "Group -2")

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if indexPath.row == 4 {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "cart", for: indexPath)
            return cell1
        }
            let cell = tableView.dequeueReusableCell(withIdentifier: "cartOrder", for: indexPath)
        return cell
    }
}
