//
//  ViewController.swift
//  restaurant
//
//  Created by iMac on 2/17/19.
//  Copyright © 2019 iMac. All rights reserved.
//
import UIKit

class TabBarControllerViewController: UITabBarController  {
    
    var id = Int()
    @objc func methodOfReceivedNotification(notification: Notification) {
        userData.Instance.fetchUser()
        if userData.Instance.token == "" || userData.Instance.token == nil {
            setupTabBar()
            self.selectedIndex = 0
        }else{
            //            setupTabBarwhenUserLogin()
            self.selectedIndex = 0
        }
    }
    @objc func methodOfReceivedNotification1(notification: Notification) {
        self.viewControllers?.remove(at: 1)
        let profileVC = storyboard?.instantiateViewController(withIdentifier: "userProfileViewController1")
        let icon1 = UITabBarItem(title:NSLocalizedString("My Profile", comment: ""), image:UIImage(named: "img23"), selectedImage:UIImage(named: "img23"))
        profileVC?.tabBarItem = icon1
        let favoriteVC = storyboard?.instantiateViewController(withIdentifier: "FavouritesViewController1")
        let icon2 = UITabBarItem(title:NSLocalizedString("My Favorite", comment: ""), image:UIImage(named: "img22"), selectedImage:UIImage(named: "img22"))
        favoriteVC?.tabBarItem = icon2
        self.viewControllers?.append(profileVC ?? UIViewController())
        self.viewControllers?.append(favoriteVC ?? UIViewController())
        self.viewControllers = viewControllers
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBar()
        //        userData.Instance.fetchUser()
        //        if userData.Instance.token == "" || userData.Instance.token == nil {
        //            setupTabBar()
        //            self.selectedIndex = 0
        //        }else{
        ////            setupTabBarwhenUserLogin()
        //            self.selectedIndex = 0
        //
        //        }
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification1(notification:)), name: Notification.Name("NotificationIdentifier1"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        userData.Instance.fetchUser()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    func setupTabBar(){
        let first = storyboard?.instantiateViewController(withIdentifier: "BranchesNV")
        first?.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "home"), selectedImage: UIImage(named: "home-1"))
        
        
        let third = AppStoryboard.Main.viewController(viewControllerClass: loginVC.self)
        let barImage: UIImage = UIImage(named: "Group 136")!.resizedImage(newWidth: 60).roundedImage.withRenderingMode(.alwaysOriginal)
        third.tabBarItem = UITabBarItem(title: "", image: barImage.withRenderingMode(.alwaysOriginal), selectedImage:barImage)
        
        let second =  AppStoryboard.Main2.viewController(viewControllerClass: BranchesVC.self)
        second.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "list"), selectedImage: #imageLiteral(resourceName: "list-1"))
        
        
     //   let fourth = storyboard?.instantiateViewController(withIdentifier: "CartN")
        
//        let story1 = UIStoryboard(name: "Main2", bundle: nil)
//        let fourth = story1.instantiateViewController(withIdentifier: "CartN")
          let fourth  = AppStoryboard.Main2.viewController(viewControllerClass: CartVC.self)

        fourth.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "cart"), selectedImage:#imageLiteral(resourceName: "cart") )
        
        
        let story = UIStoryboard(name: "Main", bundle: nil)
        let fifth = story.instantiateViewController(withIdentifier: "MYProfileN")
        fifth.tabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "user"), selectedImage: #imageLiteral(resourceName: "Group 193"))
        
        self.viewControllers = [first , second , third , fourth ,fifth] as! [UIViewController]
    }
    //    func setupTabBar(){
    //        let mainVC = storyboard?.instantiateViewController(withIdentifier: "SectionsViewController")
    //        let firstViewController = UINavigationController(rootViewController: mainVC ?? UIViewController())
    //        firstViewController.tabBarItem.image = UIImage(named: "img42")
    //        let signInVC = storyboard?.instantiateViewController(withIdentifier: "SignInViewController")
    //        let secondViewController = UINavigationController(rootViewController: signInVC ?? UIViewController())
    //        secondViewController.tabBarItem.image = UIImage(named: "img40")
    //        viewControllers = [firstViewController,secondViewController]
    //        self.setViewControllers(viewControllers, animated: true)
    //    }
    //
    //    func setupTabBarwhenUserLogin(){
    //        let mainVC = storyboard?.instantiateViewController(withIdentifier: "SectionsViewController")
    //        let firstViewController = UINavigationController(rootViewController: mainVC ?? UIViewController())
    //        firstViewController.tabBarItem.image = UIImage(named: "img42")
    //        let profileVC = storyboard?.instantiateViewController(withIdentifier: "userProfileViewController")
    //        let secondViewController = UINavigationController(rootViewController: profileVC ?? UIViewController())
    //        secondViewController.tabBarItem.image = UIImage(named: "img23")
    //        let favoriteVC = storyboard?.instantiateViewController(withIdentifier: "FavouritesViewController")
    //        let thirdViewController = UINavigationController(rootViewController: favoriteVC ?? UIViewController())
    //        thirdViewController.tabBarItem.image = UIImage(named: "img22")
    //
    //        viewControllers = [ firstViewController,secondViewController ,thirdViewController  ]
    //        self.setViewControllers(viewControllers, animated: true)
    //    }
}
