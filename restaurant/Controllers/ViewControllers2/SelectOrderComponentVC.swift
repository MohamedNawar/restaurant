//
//  SelectOrderComponentVC.swift
//  restaurant
//
//  Created by iMac on 2/20/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

class SelectOrderComponentVC: UIViewController, UITextFieldDelegate ,  UITextViewDelegate {
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var textViewView: UIView!
    @IBOutlet weak var titleForFourthView: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var fourthView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        userData.Instance.fetchUser()
        messageTextView.delegate = self
        messageTextView.text = NSLocalizedString("Write Comment", comment: "")
        messageTextView.textColor = UIColor.lightGray
    }
    @objc func myTapAction(recognizer: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textViewView.borderColor = #colorLiteral(red: 0.5663285255, green: 0.7342554331, blue: 0.9790433049, alpha: 1)
        textViewView.borderWidth = 1
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        textViewView.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        textViewView.borderWidth = 1
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.5663285255, green: 0.7342554331, blue: 0.9790433049, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
    @IBAction func Add(_ sender: Any) {
        print("asasasasasasdfsf")
    }
}
