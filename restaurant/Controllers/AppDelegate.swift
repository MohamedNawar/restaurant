//
//  AppDelegate.swift
//  restaurant
//
//  Created by iMac on 2/12/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FCAlertView
import FacebookCore
import TwitterKit

//let NormalFont = UIFont(name: "neosans_regular", size: 17)
//let BoldFont = UIFont(name: "neosans_regular", size: 17)
let mainColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    private var reachability : Reachability!
    var storyboard = UIStoryboard(name: "Main", bundle: nil)
    var navigationController = UINavigationController()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //        UserDefaults.standard.removeObject(forKey: "firstUse")
        userData.Instance.fetchUser()
        if userData.Instance.firstUse == "" || userData.Instance.firstUse == nil {
            UserDefaults.standard.set("false", forKey: "firstUse")
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController =   storyboard.instantiateViewController(withIdentifier: "TutorialViewControllerNV")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
            
        }else{
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main2", bundle: nil)
            let initialViewController =   storyboard.instantiateViewController(withIdentifier: "TabBarControllerViewController")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        // Override point for customization after application launch.
        L102Localizer.DoTheMagic()
        UIApplication.shared.statusBarStyle = .lightContent
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = mainColor
        let ToolBarAppearace = UIToolbar.appearance()
        let BarAppearace = UINavigationBar.appearance()
        //        BarAppearace.barTintColor = .clear
        BarAppearace.setBackgroundImage(UIImage(), for: .default)
        BarAppearace.shadowImage = UIImage()
        BarAppearace.isTranslucent = true
        
        ToolBarAppearace.backgroundColor = mainColor
        ToolBarAppearace.tintColor = UIColor.white
        let ButtonBarAppearace = UIBarButtonItem.appearance()
        
        ButtonBarAppearace.tintColor = UIColor.white
        
        //Keybored Setup
        IQKeyboardManager.shared.enable = true
        
        //For checking the Internet
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: ReachabilityChangedNotification, object: nil)
        self.reachability = Reachability.init()
        do {
            try self.reachability.startNotifier()
        } catch {
            
        }
        //Facebook Login
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        //Twitter Login
         TWTRTwitter.sharedInstance().start(withConsumerKey:"yejZu1lqZdwOXbNdeMN5wLX9J", consumerSecret:"iD2nu73J17xq2yLghjy0UGNHjaeW7Q2rAUVY2npvGPLMsrb9cj")

        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let twitterDidHandle = TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        let facebookDidHandle = SDKApplicationDelegate.shared.application(app, open: url, options: options)

        return facebookDidHandle || twitterDidHandle
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    //Get called when the wifi statue changed
    @objc func reachabilityChanged(notification:Notification) {
//        let reachability = notification.object as! Reachability
//        if reachability.isReachable {
//            if reachability.isReachableViaWiFi {
//                print("Reachable via WiFi")
//            } else {
//                print("Reachable via Cellular")
//            }
//        } else {
//            makeDoneAlert(title: "من فضلك قم باعادة الاتصال بالانترنت مرة اخري", SubTitle: "", Image: #imageLiteral(resourceName: "cross"), color: UIColor.red)
//            print("Network not reachable")
//        }
    }
    
    // the alert to be Poped Up
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage, color : UIColor) {
        let alert = FCAlertView()
        alert.colorScheme = color
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    
    
}

