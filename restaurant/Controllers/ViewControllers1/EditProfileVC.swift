//
//  EditProfileVC.swift
//  restaurant
//
//  Created by iMac on 2/20/19.
//  Copyright © 2019 iMac. All rights reserved.
//

    import UIKit
    import Alamofire
    import PKHUD
    import FCAlertView
    import TextFieldEffects
    class EditProfileVC: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate {
        let imagePicker = UIImagePickerController()
        var alertController = UIAlertController()
        var photoFalg = false
        @IBOutlet weak var phoneTxt: HoshiTextField!
        @IBOutlet weak var emailTxt: HoshiTextField!
        @IBOutlet weak var firstNameTxt: HoshiTextField!
        @IBOutlet weak var lastNameTxt: HoshiTextField!
        @IBOutlet weak var bProfileImg: UIImageView!
        override func viewDidLoad() {
            super.viewDidLoad()
            AlertControllerToGetImage()
            imagePicker.delegate = self

        }
        
        func AlertControllerToGetImage() {
            alertController = UIAlertController(title: "Get Image", message: "from Gallery or Camera", preferredStyle: UIAlertController.Style.actionSheet)
            let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) { (action) in
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
                
            }
            let GalleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) { (action) in
                self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                self.imagePicker.allowsEditing = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
                
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
            alertController.addAction(cameraAction)
            alertController.addAction(GalleryAction)
            alertController.addAction(cancelAction)
            
        }
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            guard let selectedImage = info[.originalImage] as? UIImage else {
                fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
            }
            bProfileImg.image = selectedImage
            photoFalg = true
            dismiss(animated: true, completion: nil)
        }
        @IBAction func saveEditing(_ sender: Any) {
        }
        @IBAction func dismiss(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
        }
        @IBAction func getPicture(_ sender: Any) {
            present(alertController, animated: true, completion: nil)
        }
}

