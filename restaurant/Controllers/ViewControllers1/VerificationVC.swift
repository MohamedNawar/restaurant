//
//  VerificationVC.swift
//  restaurant
//
//  Created by iMac on 2/17/19.
//  Copyright © 2019 iMac. All rights reserved.
//
    import UIKit
    import SVPinView
    import Alamofire
    import PKHUD
    import FCAlertView
    class VerificationVC: UIViewController {
        var code = String()
        var userToken = TempToken()
        @IBOutlet weak var pinView: SVPinView!
        @IBOutlet weak var backgroundImg: UIImageView!
        override func viewDidLoad() {
            super.viewDidLoad()
            backgroundImg.blurImage()
            configurePinView()
            pinView.style = .box
        }
        func configurePinView() {
            
            pinView.pinLength = 5
            pinView.secureCharacter = "\u{25CF}"
            pinView.interSpace = 5
            pinView.textColor = #colorLiteral(red: 0.4470946193, green: 0.6766976118, blue: 0.9975687861, alpha: 1)
            pinView.borderLineColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            pinView.activeBorderLineColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
            pinView.borderLineThickness = 1
            pinView.shouldSecureText = true
            pinView.allowsWhitespaces = false
            pinView.style = .none
            pinView.fieldBackgroundColor = UIColor.white.withAlphaComponent(0.3)
            pinView.activeFieldBackgroundColor = UIColor.white.withAlphaComponent(0.5)
            pinView.fieldCornerRadius = 25
            pinView.activeFieldCornerRadius = 25
            pinView.placeholder = "******"
            pinView.becomeFirstResponderAtIndex = 0
            
            pinView.font = UIFont.systemFont(ofSize: 15)
            pinView.keyboardType = .phonePad
            pinView.pinInputAccessoryView = { () -> UIView in
                let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
                doneToolbar.barStyle = UIBarStyle.default
                let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
                let done: UIBarButtonItem  = UIBarButtonItem(title: (NSLocalizedString("Done", comment: "")), style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
                
                var items = [UIBarButtonItem]()
                items.append(flexSpace)
                items.append(done)
                
                doneToolbar.items = items
                doneToolbar.sizeToFit()
                return doneToolbar
            }()
            
            pinView.didFinishCallback = didFinishEnteringPin(pin:)
        }
        func didFinishEnteringPin(pin:String) {
            print("The Pin entered is \(pin)")
            code = pin
            forgetPasswordStep2(pin:"\(pin)")
        }
        @objc func dismissKeyboard() {
            self.view.endEditing(false)
        }
        private func forgetPasswordStep2(pin:String){
            
            let header = APIs.Instance.getHeader()
            
            let par = ["code": "\(pin)"] as [String : String]
            print(par)
            
            HUD.show(.progress)
            Alamofire.request(APIs.Instance.forgetPassword2() , method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        do {
                            let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                            self.makeDoneAlert(title: "Error", SubTitle: err.message ?? "", Image:  #imageLiteral(resourceName: "danger"))
                            print(err.message)
                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                        do {
                            self.userToken = try JSONDecoder().decode(TempToken.self, from: response.data!)
                            print("successsss")
                            let navVC = AppStoryboard.Main.viewController(viewControllerClass: ConfirmPasswordVC.self)
                            navVC.token = self.userToken.temp_token ?? ""; self.navigationController?.pushViewController(navVC, animated: true)
                        }catch{
                            HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "")), delay: 1.0)
                        }
                        
                        
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
            }
        }
        @IBAction func setCode(_ sender: Any) {
            if code == "" || code == nil {
                HUD.flash(.label("Enter The Code"), delay: 1.0)
                return
            }else{
                if code.count > 4 {
                    forgetPasswordStep2(pin: code)
                }
            }
            
        }
        
        func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
            let alert = FCAlertView()
            alert.avoidCustomImageTint = true
            let updatedFrame = alert.bounds
            alert.colorScheme = #colorLiteral(red: 0.4470946193, green: 0.6766976118, blue: 0.9975687861, alpha: 1)
            alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: ""), andButtons: nil)
        }
}
