//
//  TutorialViewController.swift
//  YourSchools
//
//  Created by iMac on 11/8/18.
//  Copyright © 2018 iMac. All rights reserved.
// pageControl

import UIKit
class TutorialViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var skipBtn: UIButton!
    
    @IBOutlet weak var pageControl1: UIView!
    @IBOutlet weak var pageControl2: UIView!
    @IBOutlet weak var pageControl3: UIView!
    var pages = [TutorialStepViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.isDirectionalLockEnabled = true
        scrollView.isPagingEnabled = true
        let page1 = createAndAddTutorialStep("First", iconImageName: "scooter", text: "PetShare is a pet photo sharing community.")
        let page2 = createAndAddTutorialStep("Second", iconImageName: "scooter", text: "Take picture of your pet, and add filters or clipart to help them shine.")
        let page3 = createAndAddTutorialStep("Third", iconImageName: "scooter", text: "Share your photos via Facebook, email, Twitter, or instant message.")
        pages = [page1, page2, page3]
        let views: [String: UIView] = ["view": view, "page1": page1.view, "page2": page2.view, "page3": page3.view]
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[page1(==view)]|", options: [], metrics: nil, views: views)
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[page1(==view)][page2(==view)][page3(==view)]|", options: [.alignAllTop, .alignAllBottom], metrics: nil, views: views)
        NSLayoutConstraint.activate(verticalConstraints + horizontalConstraints)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        skipBtn.isHidden = false
        skipBtn.isEnabled = true; self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
    }
    private func createAndAddTutorialStep(_ backgroundImageName: String, iconImageName: String, text: String) -> TutorialStepViewController {
        let tutorialStep =
          AppStoryboard.Main.viewController(viewControllerClass: TutorialStepViewController.self) 
        tutorialStep.view.translatesAutoresizingMaskIntoConstraints = false
        tutorialStep.titleValue =  backgroundImageName
        tutorialStep.iconImage = UIImage(named: iconImageName)
        tutorialStep.text = text
        scrollView.addSubview(tutorialStep.view)
        
        addChild(tutorialStep)
        tutorialStep.didMove(toParent: self)
        
        return tutorialStep
    }
    @IBAction func skipeSteps(_ sender: Any) {

         Skip_()
        
        
    }
}

extension TutorialViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
      
        scrollView.contentOffset.y = 0
        let pageWidth = scrollView.bounds.width
        let pageFraction = scrollView.contentOffset.x / pageWidth
        print(Float(pageFraction))
        if Float(pageFraction) == 0.0  {
            pageControl1.backgroundColor = #colorLiteral(red: 0.5663285255, green: 0.7342554331, blue: 0.9790433049, alpha: 1)
            pageControl3.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.831372549, blue: 0.831372549, alpha: 1)
            pageControl2.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.831372549, blue: 0.831372549, alpha: 1)
        }
       else if Float(pageFraction) == 1.0  {
              NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
            pageControl2.backgroundColor = #colorLiteral(red: 0.5663285255, green: 0.7342554331, blue: 0.9790433049, alpha: 1)
            pageControl3.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.831372549, blue: 0.831372549, alpha: 1)
            pageControl1.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.831372549, blue: 0.831372549, alpha: 1)
        } else if Float(pageFraction) == 2.0 {
            pageControl3.backgroundColor = #colorLiteral(red: 0.5663285255, green: 0.7342554331, blue: 0.9790433049, alpha: 1)
            pageControl2.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.831372549, blue: 0.831372549, alpha: 1)
            pageControl1.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.831372549, blue: 0.831372549, alpha: 1)
        }
        if Float(pageFraction) > 2.0 {
            
            Skip_()
           
        }
    }
    
    func Skip_(){
        let navVC = AppStoryboard.Main2.viewController(viewControllerClass: TabBarControllerViewController.self)
        self.navigationController?.pushViewController(navVC, animated: true)

    }
    
}
