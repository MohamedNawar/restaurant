//
//  SendMessageVC.swift
//  restaurant
//
//  Created by iMac on 2/20/19.
//  Copyright © 2019 iMac. All rights reserved.
//

    import UIKit
    import HCSStarRatingView
    import Alamofire
    import PKHUD
    import FCAlertView
    class SendMessageVC: UIViewController , UITextFieldDelegate ,  UITextViewDelegate {
        
        @IBOutlet var backGround: UIView!
        @IBOutlet weak var messageTextView: UITextView!
        @IBOutlet weak var textViewView: UIView!
        override func viewDidLoad() {
            super.viewDidLoad()
            let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(myTapAction))
            mytapGestureRecognizer.numberOfTapsRequired = 1
            self.backGround.addGestureRecognizer(mytapGestureRecognizer)
            userData.Instance.fetchUser()
            messageTextView.delegate = self
            messageTextView.text = NSLocalizedString("Message", comment: "")
            messageTextView.textColor = UIColor.lightGray            
        }
        @objc func myTapAction(recognizer: UITapGestureRecognizer) {
            self.dismiss(animated: true, completion: nil)
            
        }
        func textViewDidBeginEditing(_ textView: UITextView) {
            textViewView.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            textViewView.borderWidth = 1
            if textView.textColor == UIColor.lightGray {
                textView.text = nil
                textView.textColor = UIColor.black
            }
            
        }
        func textViewDidEndEditing(_ textView: UITextView) {
            textViewView.borderColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 0.6377252684)
            textViewView.borderWidth = 1
        }
        func textFieldDidBeginEditing(_ textField: UITextField) {
            textField.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            textField.borderWidth = 1
        }
        func textFieldDidEndEditing(_ textField: UITextField) {
            if textField.text == "" {
                textField.borderColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 0.6377252684)
            }
        }
        @IBAction func dismiss(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
        }
        @IBAction func sendMessage(_ sender: Any) {
            
        }
}
