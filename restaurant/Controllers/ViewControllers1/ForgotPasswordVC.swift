//
//  ForgotPasswordVC.swift
//  restaurant
//
//  Created by iMac on 2/17/19.
//  Copyright © 2019 iMac. All rights reserved.
//

    import UIKit
    import Alamofire
    import PKHUD
    import FCAlertView
    import TextFieldEffects
    class ForgotPasswordVC:UIViewController {
        @IBOutlet weak var phoneTxt: HoshiTextField!
          @IBOutlet weak var backgroundImg: UIImageView!
        override func viewDidLoad() {
            super.viewDidLoad()
            backgroundImg.blurImage()

        }
        private func forgetPasswordStep1(){
            if self.phoneTxt.text == ""  {
                HUD.flash(.label(NSLocalizedString("Enter your data", comment: "")), delay: 1.0)
                return
            }
            let header = APIs.Instance.getHeader()
            
            let par = ["phone": phoneTxt.text!] as [String : String]
            print(par)
            
            HUD.show(.progress)
            Alamofire.request(APIs.Instance.forgetPassword1() , method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        do {
                            let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                            self.makeDoneAlert(title: "Error", SubTitle: err.message ?? "", Image:  #imageLiteral(resourceName: "danger"))
                            print(err.message)
                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                        do {
                            
                            print("successsss")
                            let navVC = AppStoryboard.Main.viewController(viewControllerClass: VerificationVC.self)
                            self.navigationController?.pushViewController(navVC, animated: true)
                        }catch{
                            HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "")), delay: 1.0)
                        }
                        HUD.flash(.success, delay: 1.0)
                        
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
            }
        }
        
        func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
            let alert = FCAlertView()
            alert.avoidCustomImageTint = true
            let updatedFrame = alert.bounds
            alert.colorScheme = #colorLiteral(red: 0.4470946193, green: 0.6766976118, blue: 0.9975687861, alpha: 1)
            alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: ""), andButtons: nil)
        }
        @IBAction func send(_ sender: Any) {
            forgetPasswordStep1()
        }
        
}
