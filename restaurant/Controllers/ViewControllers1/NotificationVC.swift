//
//  NotificationVC.swift
//  restaurant
//
//  Created by iMac on 2/20/19.
//  Copyright © 2019 iMac. All rights reserved.
//

    import PKHUD
    import Alamofire
    import FCAlertView
    import SDWebImage
    import HCSStarRatingView
    
    class NotificationVC: UIViewController,UITableViewDelegate, UITableViewDataSource   , UINavigationBarDelegate{
        
        @IBOutlet weak var NotificationTable: UITableView!
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            NotificationTable.separatorStyle = .none
            NotificationTable.allowsSelection = false
            userData.Instance.fetchUser()
        }
        override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "Group -2");self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "Group -2")
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
            return 7
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let  cell = NotificationTable.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath)
            
            return cell
        }
}


