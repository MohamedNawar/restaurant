//
//  TutorialStepViewController.swift
//  YourSchools
//
//  Created by iMac on 11/8/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit

class TutorialStepViewController: UIViewController {
    
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var textLabel: UILabel!
    
    @IBOutlet weak var titleLbl: UILabel!
    var titleValue: String?
    var iconImage: UIImage?
    var text: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
 self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        titleLbl.text  = titleValue
        iconImageView.image = iconImage
        textLabel.text = text

    }
 
    @IBAction func skipeSteps(_ sender: Any) {
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: TabBarControllerViewController.self)
        self.navigationController?.pushViewController(navVC, animated: true)

    }
}

