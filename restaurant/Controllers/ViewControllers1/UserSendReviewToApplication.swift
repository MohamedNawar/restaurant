//
//  UserSendReviewToApplication.swift
//  restaurant
//
//  Created by iMac on 2/20/19.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit
import HCSStarRatingView
import Alamofire
import PKHUD
import FCAlertView
class UserSendReviewToApplication: UIViewController , UITextFieldDelegate ,  UITextViewDelegate {
  
    @IBOutlet var backGround: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var textViewView: UIView!
    @IBOutlet weak var rattingView: HCSStarRatingView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(myTapAction))
        mytapGestureRecognizer.numberOfTapsRequired = 1
        self.backGround.addGestureRecognizer(mytapGestureRecognizer)
        userData.Instance.fetchUser()
        rattingView.value = CGFloat(0)
        messageTextView.delegate = self
        messageTextView.text = NSLocalizedString("Write Comment", comment: "")
        messageTextView.textColor = UIColor.lightGray
        self.title = NSLocalizedString("Send Review", comment:"أرسال تقييم")
        
    }
    @objc func myTapAction(recognizer: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textViewView.borderColor = #colorLiteral(red: 0.5663285255, green: 0.7342554331, blue: 0.9790433049, alpha: 1)
        textViewView.borderWidth = 1
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        textViewView.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        textViewView.borderWidth = 1
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.5663285255, green: 0.7342554331, blue: 0.9790433049, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func sendMessage(_ sender: Any) {
        
    }
}
