//
//  loginVC.swift
//  restaurant
//
//  Created by iMac on 2/17/19.
//  Copyright © 2019 iMac. All rights reserved.
//
import UIKit
import Alamofire
import PKHUD
import FCAlertView
import TextFieldEffects
import FacebookCore
import FacebookLogin
import TwitterKit
import TwitterCore

class loginVC: UIViewController {
    var shouldBack = String()
    var signinTag = Int()
    @IBOutlet weak var passwordTxt: HoshiTextField!
    @IBOutlet weak var backgroundImg: UIImageView!
    @IBOutlet weak var phoneTxt: HoshiTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImg.blurImage()
    }

    @IBAction func SignIn(_ sender: Any) {
        signinTag = 0
        userLogin(token: "")
    }
    @IBAction func SignUp(_ sender: Any) {
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: SignupVC.self)
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func signInWithFacebook(_ sender: Any) {
        signinTag = 1
        facebook_login()
    }
    @IBAction func signInWithTwitter(_ sender: Any) {
        signinTag = 2
        twitter_login()
    }

    private func userLogin(token:String){
        let header = APIs.Instance.getHeader()
        var par = ["access_token": "\(token)"] as [String : String]
        var url = APIs.Instance.loginFacebook()
        print(par)
        HUD.show(.progress)
        switch signinTag {
        case 0:
            //login with email

            print("login with email")
            if self.phoneTxt.text == "" || self.passwordTxt.text == ""  {
                HUD.flash(.label(NSLocalizedString("Enter your data", comment: "")), delay: 1.0)
                return
            }
            par = ["phone_number": phoneTxt.text!.replacedArabicDigitsWithEnglish, "password": passwordTxt.text!.replacedArabicDigitsWithEnglish] as [String : String]
            print(par)
            url = APIs.Instance.login()

        case 1:
            //login with facebook
            print("login with facebook")
            par = ["access_token": "\(token)"] as [String : String]
            url = APIs.Instance.loginFacebook()

        case 2:
            //login with twitter
            print("login with twitter")
            url = APIs.Instance.loginTwitter()
            //TODO

        default:
            print("Default")
        }

        Alamofire.request(url , method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                //HUD.hide()
                //print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.errors?.first ?? "", Image:  #imageLiteral(resourceName: "home"))
                        print(err.message)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        userData.Instance.saveUser(data: response.data!)
                        userData.Instance.fetchUser()
                        print(userData.Instance.data?.name ?? "")
                        print("successsss")
                        HUD.hide()
                    }catch{
                        HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "حدث خطأ برجاء اعادة المحاولة")), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    private func twitter_login(){
        TWTRTwitter.sharedInstance().logIn { [weak self] (session, error) in
            guard let self = self  else { return }
            if error != nil {
                print("Error twitter")
                return
            }
            self.userLogin(token: "\(session?.authToken ?? "")")
            print("signed in as \( session?.userName ?? "") ")
            print("userID \(session?.userID ?? "")" )
            print("authToken \(session?.authToken ?? "")")
        }
    }
    private func facebook_login(){
        let login_manager = LoginManager()
        login_manager.logIn(readPermissions: [.publicProfile, .email], viewController: self){ (result) in
            switch result{
            case .success(grantedPermissions: _, declinedPermissions: _, token: _):
                print("facebook logged in")

                if AccessToken.current != nil {
                    if let token = AccessToken.current?.authenticationToken {
                        print("token is \(token)")
                        self.userLogin(token: "\(token)")
                    }
                }
            case .failed(let err):
                print(err)
                print("failed")
            case .cancelled:
                print("canceled")
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        let updatedFrame = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.4470946193, green: 0.6766976118, blue: 0.9975687861, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func forgotPassword(_ sender: Any) {
        let navVC = AppStoryboard.Main.viewController(viewControllerClass: ForgotPasswordVC.self)
        self.navigationController?.pushViewController(navVC, animated: true)    }
}

