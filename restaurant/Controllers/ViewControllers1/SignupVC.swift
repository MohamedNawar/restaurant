//
//  SignupVC.swift
//  restaurant
//
//  Created by iMac on 2/17/19.
//  Copyright © 2019 iMac. All rights reserved.
//

    import UIKit
    import Alamofire
    import PKHUD
    import FCAlertView
    import TextFieldEffects
    class SignupVC: UIViewController {
        var shouldBack = String()
   
        @IBOutlet weak var confirmPasswordTxt: HoshiTextField!
        @IBOutlet weak var passwordTxt: HoshiTextField!
        @IBOutlet weak var phoneTxt: HoshiTextField!
        @IBOutlet weak var emailTxt: HoshiTextField!
        @IBOutlet weak var firstNameTxt: HoshiTextField!
        @IBOutlet weak var lastNameTxt: HoshiTextField!
        
        @IBOutlet weak var backgroundImg: UIImageView!
        override func viewDidLoad() {
            super.viewDidLoad()
            backgroundImg.blurImage()
        }
        
        private func userSignUp(){
            if self.emailTxt.text == "" || self.confirmPasswordTxt.text == "" || self.passwordTxt.text == "" || self.phoneTxt.text == "" || self.firstNameTxt.text == "" || self.lastNameTxt.text == ""  {
                HUD.flash(.label(NSLocalizedString("Enter your data", comment: "")), delay: 1.0)
                return
            }
            if self.passwordTxt.text != self.confirmPasswordTxt.text {
                HUD.flash(.label(NSLocalizedString("Password not identical", comment: "")), delay: 1.0)
                return
                
            }
            let header = APIs.Instance.getHeader()
            
            let par = ["email": emailTxt.text! , "phone": emailTxt.text!.replacedArabicDigitsWithEnglish, "password": passwordTxt.text!.replacedArabicDigitsWithEnglish , "name" : firstNameTxt.text!] as [String : String]
            print(par)
            HUD.show(.progress, onView: self.view)
            Alamofire.request(APIs.Instance.registeration(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                print(response)
                switch(response.result) {
                case .success(let value):
                    HUD.hide()
                    let temp = response.response?.statusCode ?? 400
                    print(temp)
                    if temp >= 300 {
                        print("errorrrr")
                        do {
                            let err = try JSONDecoder().decode(ErrorData.self, from: response.data!)
                            self.makeDoneAlert(title: "Error", SubTitle: err.errors?.first ?? "", Image:  #imageLiteral(resourceName: "danger"))
                            print(err.message)
                        }catch{
                            print("errorrrrelse")
                            
                        }
                    }else{
                        
                        do {
                            userData.Instance.saveUser(data: response.data!)
                            userData.Instance.fetchUser()
                            
                            print("successsss")
//                            self.navigationController?.popViewController(animated: true)
//                            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                            
                        }catch{
                            HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "")), delay: 1.0)
                        }
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
            }
            
        }
        
        func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
            let alert = FCAlertView()
            alert.avoidCustomImageTint = true
            let updatedFrame = alert.bounds
            alert.colorScheme = #colorLiteral(red: 0.4980392157, green: 0.6705882353, blue: 0.9725490196, alpha: 1)
            alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: ""), andButtons: nil)
        }

        @IBAction func SignUp(_ sender: Any) {
            userSignUp()
        }
}

