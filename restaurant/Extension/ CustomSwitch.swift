//
//  ViewController.swift
//  Scarfi
//
//  Created by iMac on 11/19/18.
//  Copyright © 2018 iMac. All rights reserved.
//
import UIKit
@IBDesignable

class UISwitchCustom: UISwitch {
    @IBInspectable var OffTint: UIColor? {
        didSet {
            self.tintColor = OffTint
            self.layer.cornerRadius = 16
            self.backgroundColor = OffTint
        }
    }
}

